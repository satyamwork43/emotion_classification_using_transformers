!pip install datasets
!pip install transformers
!pip install git+https://github.com/huggingface/datasets.git
!pip install git+https://github.com/huggingface/transformers.git
!pip install torchaudio
!pip install librosa
!pip install mutagen


import pandas as pd
import glob
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchaudio
from transformers import AutoConfig, Wav2Vec2FeatureExtractor
import librosa
import IPython.display as ipd
import numpy as np
import pandas as pd

from dataclasses import dataclass
from typing import Optional, Tuple
import torch
from transformers.file_utils import ModelOutput
import torch.nn as nn
from torch.nn import BCEWithLogitsLoss, CrossEntropyLoss, MSELoss

from transformers.models.wav2vec2.modeling_wav2vec2 import (
    Wav2Vec2PreTrainedModel,
    Wav2Vec2Model
)

import numpy as np
import pandas as pd
import os
from datasets import load_dataset, Audio

from transformers import AutoFeatureExtractor
from transformers import AutoModelForAudioClassification, TrainingArguments, Trainer

# model_checkpoint = "superb/hubert-base-superb-er"

from sklearn.metrics import accuracy_score, f1_score, recall_score, precision_score

from sklearn.model_selection import train_test_split

import glob
import torch
import gc
from datasets import load_metric
from dataclasses import dataclass
from typing import Dict, List, Optional, Union

import torch
from transformers import Wav2Vec2Processor
from transformers import Wav2Vec2FeatureExtractor
from datasets import DatasetDict, load_dataset, load_metric
import pickle
from tensorflow.keras import backend as K
import librosa

import tensorflow as tf
import mutagen
from mutagen.wave import WAVE
from tqdm import tqdm
from transformers import HubertForSequenceClassification, Wav2Vec2FeatureExtractor

from transformers import AutoConfig



labels = ['wav_neutral','wav_fear']

label2id, id2label = dict(), dict()

for i, label in enumerate(labels):
    label2id[label] = str(i)
    id2label[str(i)] = label

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# model_name_or_path = "lighteternal/wav2vec2-large-xlsr-53-greek"
model_name_or_path = "lighteternal/wav2vec2-large-xlsr-53-greek"

config = AutoConfig.from_pretrained(model_name_or_path)
feature_extractor = Wav2Vec2FeatureExtractor.from_pretrained(model_name_or_path)
sampling_rate = feature_extractor.sampling_rate
model = Wav2Vec2ForSpeechClassification.from_pretrained('/content/results/best').to(device)

def speech_file_to_array_fn(path, sampling_rate):
    speech_array, _sampling_rate = torchaudio.load(path)
    resampler = torchaudio.transforms.Resample(_sampling_rate)
    speech = resampler(speech_array).squeeze().numpy()
    return speech
def predict(path, sampling_rate):
    speech = speech_file_to_array_fn(path, sampling_rate)
    inputs = feature_extractor(speech, sampling_rate=sampling_rate, return_tensors="pt", padding=True)
    inputs = {key: inputs[key].to(device) for key in inputs}
    with torch.no_grad():
        logits = model(**inputs).logits
    scores = F.softmax(logits, dim=1).detach().cpu().numpy()[0]
    outputs = [{"Emotion": config.id2label[i], "Score": f"{round(score * 100, 3):.1f}%"} for i, score in enumerate(scores)]
    return outputs

path = '/content/l_1411158100_KRAP7OEJFP6CH36CPD55NIOI3K016M6T-0_3089537_2022-02-16_16-12-42_282726.wav57.wav'
outputs = predict(path, sampling_rate)

outputs

path = '/content/l_Abhishek.Aspat__NayanPSFOutboundCampaign__16__-1__7005539020__2022-09-26_11-20-47.wav81.wav_neutral.wav'
outputs = predict(path, sampling_rate)
outputs

for i in glob.glob('/content/drive/MyDrive/satyam_fumblimg_detection/audios2/new_167_calls/'+'*.wav'):
  # path = '/content/l_1411158100_KRAP7OEJFP6CH36CPD55NIOI3K016M6T-0_3089537_2022-02-16_16-12-42_282726.wav57.wav'
  print(i)
  outputs = predict(i, sampling_rate)
  print(outputs)
  print('\n')

