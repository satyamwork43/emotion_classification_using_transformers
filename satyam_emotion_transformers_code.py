!pip install datasets
!pip install transformers
!pip install git+https://github.com/huggingface/datasets.git
!pip install git+https://github.com/huggingface/transformers.git
!pip install torchaudio
!pip install librosa
!pip install mutagen


import pandas as pd
import glob
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchaudio
from transformers import AutoConfig, Wav2Vec2FeatureExtractor
import librosa
import IPython.display as ipd
import numpy as np
import pandas as pd

from dataclasses import dataclass
from typing import Optional, Tuple
import torch
from transformers.file_utils import ModelOutput
import torch.nn as nn
from torch.nn import BCEWithLogitsLoss, CrossEntropyLoss, MSELoss

from transformers.models.wav2vec2.modeling_wav2vec2 import (
    Wav2Vec2PreTrainedModel,
    Wav2Vec2Model
)

import numpy as np
import pandas as pd
import os
from datasets import load_dataset, Audio

from transformers import AutoFeatureExtractor
from transformers import AutoModelForAudioClassification, TrainingArguments, Trainer

# model_checkpoint = "superb/hubert-base-superb-er"

from sklearn.metrics import accuracy_score, f1_score, recall_score, precision_score

from sklearn.model_selection import train_test_split

import glob
import torch
import gc
from datasets import load_metric
from dataclasses import dataclass
from typing import Dict, List, Optional, Union

import torch
from transformers import Wav2Vec2Processor
from transformers import Wav2Vec2FeatureExtractor
from datasets import DatasetDict, load_dataset, load_metric
import pickle
from tensorflow.keras import backend as K
import librosa

import tensorflow as tf
import mutagen
from mutagen.wave import WAVE
from tqdm import tqdm
from transformers import HubertForSequenceClassification, Wav2Vec2FeatureExtractor

from transformers import AutoConfig

path = []
emotion = []
for i in glob.glob('/content/drive/MyDrive/satyam_fumblimg_detection/audios/training_28feb6/'+'*.wav'):
  path.append(i)
  emotion.append(i.split('.')[-2])
df = pd.DataFrame({'Filepath':path,'Label':emotion})
df = df[df['Label']!='wav0_neutral']
df = df[df['Label']!='wav_neutral (copy)']

@dataclass
class SpeechClassifierOutput(ModelOutput):
    loss: Optional[torch.FloatTensor] = None
    logits: torch.FloatTensor = None
    hidden_states: Optional[Tuple[torch.FloatTensor]] = None
    attentions: Optional[Tuple[torch.FloatTensor]] = None


class Wav2Vec2ClassificationHead(nn.Module):
    """Head for wav2vec classification task."""

    def __init__(self, config):
        super().__init__()
        self.dense = nn.Linear(config.hidden_size, config.hidden_size)
        self.dropout = nn.Dropout(config.final_dropout)
        self.out_proj = nn.Linear(config.hidden_size, config.num_labels)

    def forward(self, features, **kwargs):
        x = features
        x = self.dropout(x)
        x = self.dense(x)
        x = torch.tanh(x)
        x = self.dropout(x)
        x = self.out_proj(x)
        return x

class Wav2Vec2ForSpeechClassification(Wav2Vec2PreTrainedModel):
    def __init__(self, config):
        super().__init__(config)
        self.num_labels = config.num_labels
        self.pooling_mode = config.pooling_mode
        self.config = config

        self.wav2vec2 = Wav2Vec2Model(config)
        self.classifier = Wav2Vec2ClassificationHead(config)

        self.init_weights()

    def freeze_feature_extractor(self):
        self.wav2vec2.feature_extractor._freeze_parameters()

    def merged_strategy(
            self,
            hidden_states,
            mode="mean"
    ):
        if mode == "mean":
            outputs = torch.mean(hidden_states, dim=1)
        elif mode == "sum":
            outputs = torch.sum(hidden_states, dim=1)
        elif mode == "max":
            outputs = torch.max(hidden_states, dim=1)[0]
        else:
            raise Exception(
                "The pooling method hasn't been defined! Your pooling mode must be one of these ['mean', 'sum', 'max']")

        return outputs

    def forward(
            self,
            input_values,
            attention_mask=None,
            output_attentions=None,
            output_hidden_states=None,
            return_dict=None,
            labels=None,
    ):
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict
        outputs = self.wav2vec2(
            input_values,
            attention_mask=attention_mask,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
            return_dict=return_dict,
        )
        hidden_states = outputs[0]
        hidden_states = self.merged_strategy(hidden_states, mode=self.pooling_mode)
        logits = self.classifier(hidden_states)

        loss = None
        if labels is not None:
            if self.config.problem_type is None:
                if self.num_labels == 1:
                    self.config.problem_type = "regression"
                elif self.num_labels > 1 and (labels.dtype == torch.long or labels.dtype == torch.int):
                    self.config.problem_type = "single_label_classification"
                else:
                    self.config.problem_type = "multi_label_classification"

            if self.config.problem_type == "regression":
                loss_fct = MSELoss()
                loss = loss_fct(logits.view(-1, self.num_labels), labels)
            elif self.config.problem_type == "single_label_classification":
                loss_fct = CrossEntropyLoss()
                loss = loss_fct(logits.view(-1, self.num_labels), labels.view(-1))
            elif self.config.problem_type == "multi_label_classification":
                loss_fct = BCEWithLogitsLoss()
                loss = loss_fct(logits, labels)

        if not return_dict:
            output = (logits,) + outputs[2:]
            return ((loss,) + output) if loss is not None else output

        return SpeechClassifierOutput(
            loss=loss,
            logits=logits,
            hidden_states=outputs.hidden_states,
            attentions=outputs.attentions,
        )



def speech_file_to_array_fn(path, sampling_rate):
    speech_array, _sampling_rate = torchaudio.load(path)
    resampler = torchaudio.transforms.Resample(_sampling_rate)
    speech = resampler(speech_array).squeeze().numpy()
    return speech
def predict(path, sampling_rate):
    speech = speech_file_to_array_fn(path, sampling_rate)
    inputs = feature_extractor(speech, sampling_rate=sampling_rate, return_tensors="pt", padding=True)
    inputs = {key: inputs[key].to(device) for key in inputs}
    with torch.no_grad():
        logits = model(**inputs).logits
    scores = F.softmax(logits, dim=1).detach().cpu().numpy()[0]
    outputs = [{"Emotion": config.id2label[i], "Score": f"{round(score * 100, 3):.1f}%"} for i, score in enumerate(scores)]
    return outputs


labels = ['wav_neutral','wav_fear']

label2id, id2label = dict(), dict()

for i, label in enumerate(labels):
    label2id[label] = str(i)
    id2label[str(i)] = label


gc.collect()

torch.cuda.empty_cache()


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# model_name_or_path = "harshit345/xlsr-wav2vec-speech-emotion-recognition"
model_name_or_path = "m3hrdadfi/wav2vec2-xlsr-persian-speech-emotion-recognition"

config = AutoConfig.from_pretrained(model_name_or_path)
feature_extractor = Wav2Vec2FeatureExtractor.from_pretrained(model_name_or_path)
sampling_rate = feature_extractor.sampling_rate
model = Wav2Vec2ForSpeechClassification.from_pretrained(model_name_or_path,num_labels=2, label2id=label2id, id2label=id2label,ignore_mismatched_sizes=True).to(device)

# df = df.sample(500)

df['Label'] = df['Label'].replace({'wav_neutral':0,'wav_fear':1})


# function to convert the information into
# some readable format
def audio_duration(length):
	hours = length // 3600 # calculate in hours
	length %= 3600
	mins = length // 60 # calculate in minutes
	length %= 60
	seconds = length # calculate in seconds

	return seconds # returns the duration

# Create a WAVE object
# Specify the directory address of your wavpack file
# "alarm.wav" is the name of the audiofile
for idx,dt in df.iterrows():
  audio = WAVE(dt[0])

  audio_info = audio.info
  length = int(audio_info.length)
  seconds = audio_duration(length)
  if seconds >5:
    print(dt[0])
    df.drop(idx,inplace=True)


df=df.sample(frac=1)

# import time
# # time.time() gives you the current time
# # os.path.getmtime(path) give you the modified time for a file
# for idx,filename in df.iterrows():

#     modified_time=os.path.getmtime(filename[0])
#     print(modified_time)
#     # if time.time()-modified_time > 300: #time in seconds
#     #     df.drop(idx,inplace=True)

# df

# !pip install vaex

# df1 = df.sample(100)

# from nlp import load_dataset

# dset = load_dataset("text", "/path/to/file.txt")["train"]
# dset.set_format("tensorflow", columns=["text"])

# def dataset_gen():
#     for ex in dset:
#         yield ex
# tf_dataset = tf.data.Dataset.from_generator(dataset_gen, output_types={"text": tf.string})

# for i in glob.glob(path+'*.wav'):
#   file_name.append(i)
#   label.append(i.split('.')[-2].replace('wav_neutral (copy)','wav_neutral').replace('wav0_neutral','wav_neutral'))
# data = pd.DataFrame({'Filepath':file_name,'Label':label})

# data['Label'] = data['Label'].replace({'wav_neutral':0,'wav_fear':1})

# data = data.sample(100)


audio_array=[]

for i in tqdm(df['Filepath']):
  audio_array.append(librosa.load(i, sr=16000, mono=False)[0])


input = feature_extractor(
    raw_speech=audio_array,
    # feature_size = 1,
    sampling_rate=16000,
    padding=True,
    return_tensors="pt"
    )
K.clear_session()


NUM_LABELS = 2

# cache_dir="/home/ubuntu/drive_b/Satyam_Emotion_Transformer/input"
df.to_csv('train.csv',index=False)
dataset = load_dataset('csv', data_files={'train': 'train.csv',},cache_dir="input")
# dataset = dataset.cast_column("Filepath", Audio(sampling_rate=16000))
torch.cuda.empty_cache()
ds = dataset.map(
        lambda x: {
            "array": librosa.load(x["Filepath"], sr=16000, mono=False)[0]
        },
        num_proc=4,
    )

INPUT_FIELD = "input_values"
LABEL_FIELD = "labels"

def prepare_dataset(batch, feature_extractor):
    audio_arr = batch["array"]
    input = feature_extractor(
        audio_arr, sampling_rate=16000, padding=True, return_tensors="pt"
    )

    batch[INPUT_FIELD] = input.input_values[0]
    batch[LABEL_FIELD] = batch[
        "Label"
    ]

    return batch

ds = ds.map(
    prepare_dataset,
    fn_kwargs={"feature_extractor": feature_extractor},
)



pickle.dump(ds, open('model.pkl', 'wb'))

ds = pickle.load(open('model.pkl', 'rb'))

ds = ds.class_encode_column("Label")


train_testvalid = ds["train"].train_test_split(shuffle=True, test_size=0.1)
# Split the 10% test + valid in half test, half valid
test_valid = train_testvalid['test'].train_test_split(test_size=0.1)
# gather everyone if you want to have a single DatasetDict
ds = DatasetDict({
    'train': train_testvalid['train'],
    'test': test_valid['test'],
    'val': test_valid['train']})

# from transformers import Wav2Vec2FeatureExtractor
# feature_extractor = Wav2Vec2FeatureExtractor()
# print(feature_extractor)

# from transformers import Wav2Vec2FeatureExtractor
# feature_extractor = Wav2Vec2FeatureExtractor(
#           sampling_rate=24000,
#           truncation=True
# )
# print(feature_extractor)


# model = "superb/hubert-base-superb-er"
# feature_extractor = Wav2Vec2FeatureExtractor.from_pretrained(model)

trainer_config = {
  "OUTPUT_DIR": "results",
  "TRAIN_EPOCHS": 50,
  "TRAIN_BATCH_SIZE": 4,
  "EVAL_BATCH_SIZE": 4,
  "GRADIENT_ACCUMULATION_STEPS": 4,
  "WARMUP_STEPS": 500,
  "DECAY": 0.01,
  "LOGGING_STEPS": 10,
  "MODEL_DIR": "superb/hubert-base-superb-er",
  "SAVE_STEPS": 100
}

training_args = TrainingArguments(
    output_dir='results',
    evaluation_strategy="epoch",
    save_strategy="epoch",
    num_train_epochs=1,
    report_to=None,
    load_best_model_at_end=True,
    save_total_limit=1,
    metric_for_best_model='loss',
    per_device_train_batch_size = 4,
    per_device_eval_batch_size = 4,
    )

INPUT_FIELD = "input_values"
LABEL_FIELD = "labels"


@dataclass
class DataCollatorCTCWithPadding:
    processor: Wav2Vec2Processor
    padding: Union[bool, str] = True
    max_length: Optional[int] = None
    max_length_labels: Optional[int] = None
    pad_to_multiple_of: Optional[int] = None
    pad_to_multiple_of_labels: Optional[int] = None

    def __call__(
        self, examples: List[Dict[str, Union[List[int], torch.Tensor]]]
    ) -> Dict[str, torch.Tensor]:

        input_features = [
            {INPUT_FIELD: example[INPUT_FIELD]} for example in examples
        ]  # example is basically row0, row1, etc...
        labels = [example[LABEL_FIELD] for example in examples]

        batch = self.processor.pad(
            input_features,
            padding=self.padding,
            max_length=self.max_length,
            pad_to_multiple_of=self.pad_to_multiple_of,
            return_tensors="pt",
        )
        batch[LABEL_FIELD] = torch.tensor(labels)

        return batch

# model_id = "superb/hubert-base-superb-er"
# feature_extractor = Wav2Vec2FeatureExtractor.from_pretrained(model_id)

data_collator = DataCollatorCTCWithPadding(
            processor=feature_extractor,
            padding=True
)


def compute_metrics(eval_pred):
    # DEFINE EVALUATION METRIC
    compute_accuracy_metric = load_metric("accuracy")
    logits, labels = eval_pred
    predictions = np.argmax(logits, axis=-1)
    return compute_accuracy_metric.compute(predictions=predictions, references=labels)

# !pip install wandb
trainer = Trainer(
    model=model,  # the instantiated 🤗 Transformers model to be trained
    args=training_args,  # training arguments, defined above
    data_collator=data_collator,
    train_dataset=ds["train"],  # training dataset
    eval_dataset=ds["val"],  # evaluation dataset
    compute_metrics=compute_metrics,
)

# os.environ["WANDB_DISABLED"] = "true"

trainer.train()

trainer.save_model(os.path.join('results', "best"))